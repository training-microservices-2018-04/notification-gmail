package com.artivisi.training.microservice201804.notificationgmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationGmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificationGmailApplication.class, args);
	}
}
