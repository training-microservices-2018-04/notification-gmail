package com.artivisi.training.microservice201804.notificationgmail.service;

import com.artivisi.training.microservice201804.notificationgmail.dto.EmailRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KafkaListenerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Autowired
    private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.email.request}")
    public void terimaEmailRequest(String msg) {
        LOGGER.debug("Terima message [{}]", msg);
        try {
            EmailRequest emailRequest = objectMapper.readValue(msg, EmailRequest.class);

            LOGGER.debug("From : "+emailRequest.getFrom());
            LOGGER.debug("To : "+emailRequest.getTo());
            LOGGER.debug("Subject : "+emailRequest.getSubject());
            LOGGER.debug("Content : "+emailRequest.getContent());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
